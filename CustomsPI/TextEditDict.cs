﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class TextEditDict : UserControl
    {
        public event EventHandler ButtonClick;


        public Image image
        {
            get { return this.pictureButton.Image; }
            set { pictureButton.Image = value; }
        }

        public TextEditDict()
        {
            InitializeComponent();
            
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            if (pictureButton.BackColor == Color.FromArgb(218, 221, 254))
            {
                e.Graphics.DrawRectangle(new Pen(Color.FromArgb(131, 143, 252)), 0, 0, pictureButton.Width - 1, pictureButton.Height - 1);
            }
        }

        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
            pictureButton.BackColor = Color.FromArgb(218, 221, 254);
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            pictureButton.BackColor = SystemColors.Control;
        }

        private void pictureButton_Click(object sender, EventArgs e)
        {
            if (this.ButtonClick != null)
                this.ButtonClick(this, e);
        }


        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F4 && this.ButtonClick != null)
            {
                this.ButtonClick(this, new EventArgs());
            }
        }
    }
}
